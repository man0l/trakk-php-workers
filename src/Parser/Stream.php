<?php

namespace Trakk\Parser;

class Stream {

    private $url;
    private $context;
    private $opts;
    private $cookiesToSend = array();
    
    function __construct($url) {
    
        $this->url = $url;
        $this->initContext();
    }
    
    function setOpts($opts) {
        
        if(!$opts) {
        $this->opts = array(
            'http' => array(
                'method'=> 'GET',
                'header' => 'Accept-language: en\r\n',
                "User-Agent: Mozilla/5.0 (Windows NT 6.1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/41.0.2228.0 Safari/537.36\r\n",
                'follow_location' => 1                
            )
        );
        } else $this->opts = $opts;
        
    }
    
    function getOpts() 
    {
        
        return $this->opts;
        
    }
    
    function initContext($opts = null) {
        
        $this->context = stream_context_create($this->getOpts());
        
        
        $this->readCookieFirst();
    }
    
    function readCookieFirst() {
        
        $regexURL = "/http:\/\/[a-z0-9\.]*/is";
        
        if(preg_match($regexURL, $this->url, $matchUrl))
        {
            $fp = fopen($matchUrl[0], 'r', false, $this->context);
            
            if(!$fp) {
                throw new Exception('The resource could not be opened');
            }
            
            $cookiesToSend = array();
        
            if(isset($http_response_header)) {
                $regexCookie = "/set-cookie:\s(.*?)=(.*?);/is";
                
                foreach($http_response_header as $header) {
                    if(preg_match($regexCookie, $header, $matchCookie))
                    {
                        $cookiesToSend[] = sprintf("%s=%s", $matchCookie[1],$matchCookie[2]);
                    }
                }
            }
            
            fclose($fp);
        
            
            $this->cookiesToSend = $cookiesToSend;
        }
         
        

    }
        
    function read() {
    
        $currentOpts = $this->getOpts();
        
        $header = "Accept-language: en\r\n";
        
        if($this->cookiesToSend)
        {
            $header .= sprintf("Cookie: %s\r\n",implode(";", $this->cookiesToSend));
        }
        
        $currentOpts['http']['header'] = $header;

        $this->setOpts($currentOpts);
        
        $this->context = stream_context_create($this->getOpts());
        $fp = fopen($this->url, 'r', false, $this->context);
        
        if(!$fp) {
            throw new Exception('The resource could not be opened');
        }
        
         
       while(!feof($fp))
       {
            $chunk = fread($fp, 2048);
             
            if(!$chunk) {
              throw new Exception('The resource could not be opened');
            } 
             
             
            $regexPrice = '/<span.*?a-color-price">\$([0-9\.]*)<\/span>/i';
            $regexFBA = '/<div id="merchant-info.*?>.*(Ships from and sold by Amazon\.com\.).*<\/div>/is';
            $regexFBA2 = '/<div id="merchant-info.*?>.*?(fulfilled by amazon)/is';
            $regexInStock = '/<div id="availability.*?>.*?<span.*?>.*(in stock\.)/is';
            
            // another layout
            
            $regexPrice2 = '/<span id="actualPriceValue">.*?\$([0-9\.]+)/i';
            $regexInStock2 = '/<div id="availability_feature_div".*?(in stock)/is';
            $regexInStock3 = "/<span class=\"availGreen\">(.*)?(In Stock).*\.<\/span>/i";
            $regexFBA3 = '/<div id="availability_feature_div".*?(ships from and sold by <b>amazon.com<\/b>)/is';
            $regexFBA31 = '/<br \/> Ships from and sold by <b>Amazon\.com<\/b>/';
            
            $regexFBA4 = '/sold by.*fulfilled by Amazon/i';
            
            $price = 0;
            $inPrime = 0;
            $inStock = 0;
            
            if(preg_match($regexPrice, $chunk, $matchPrice))    
            {
                
                 $price = $matchPrice[1];

            } 
            
            if (preg_match_all($regexFBA, $chunk, $matchFBA))             
            {
                $inPrime = 1;

            } 

            if (preg_match_all($regexFBA2, $chunk, $matchFBA2))
            {
                $inPrime = 1;

            } 

            if (preg_match_all($regexInStock, $chunk, $matchInStock))
            {
                  $inStock = 1;

            } 
            
            if(preg_match_all($regexInStock3, $chunk, $matchInStock3)) {
                  $inStock = 1;
            }

            if(preg_match($regexPrice2, $chunk, $matchPrice2)) 
            {
                    
                    $price = $matchPrice[1];

            } 

            if(preg_match_all($regexInStock2, $chunk, $matchInStock2)) 
            {
                $inStock = 1;

            } 

            if(preg_match_all($regexFBA3, $chunk, $matchFBA3)) 
            {
                $inPrime = 1;

            } 
            
             if(preg_match_all($regexFBA31, $chunk, $matchFBA31)) 
            {
                $inPrime = 1;

            } 

            if(preg_match_all($regexFBA4, $chunk, $matchFBA4)) 
            {
                $inPrime = 1;

            }
            
            
            
            
       }
       
       fclose($fp);
        
        
    }
    
}