<?php

namespace Trakk\Storage;

use Trakk\Storage\StorageInterface;
/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


abstract class AbstractStorage implements StorageInterface
{
    private $content;
    private $fileName;

    public function setSaveDir($saveDir) {
        $this->saveDir = $saveDir;
        return $this;
    }
    
    public function getSaveDir()
    {
        return $this->saveDir;
    }
    
    public function setContent($content)    
    {
        $this->content = $content;
        return $this;
    }
    
    public function getContent()
    {   
        return $this->content;
        
    }
    
    public function getFileName()
    {
        return $this->fileName;
    }
    
    public function isHTML()
    {
        $isHTML = false;
        
        if(isset($this->content))
        {
            $regex = "/<html.*?>/";
            if(preg_match($regex, $this->content))
            {
                $isHTML = true;
            }
        }
        
        return $isHTML;
        
    }
    
    abstract public function save();

}