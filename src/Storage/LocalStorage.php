<?php
 
 namespace Trakk\Storage;
 
 use Trakk\Storage\AbstractStorage;
/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

class LocalStorage extends AbstractStorage
{
    /**
     * @desc please keep the current dir with absolute path
     * @var string $saveDir
     */
    private $saveDir = '/tmp';
    
    public function save()
    {
        if(!isset($this->content))
        {
            throw new Exception('Please add some HTML content through the setContent method');
        }
        
        if(!isset($this->saveDir))
        {
            throw new Exception('Please set saveDir param by setSaveDir method');
        }
        
        if(!is_dir($this->saveDir))
        {
            try {
                mkdir($this->saveDir, 644);
            } catch (Exception $ex) {
                throw $ex;
            }
        }
        
        if(!$this->isHTML())
        {
            throw new Exception("The content setted up in setContent method isn\'t in HTML format");
        }
        
        $this->fileName = sprintf("%s.html", md5($content));    
        $filePath = $this->saveDir . DIRECTORY_SEPARATOR . $this->fileName;
        
        $fp = fopen($filePath, 'w');
        
        if(!$fp)
        {
            throw new Exception("The file $filePath could not be opened.");
        }
        
        fwrite($fp, $this->content);
        fclose($fp);
        
        return $this->fileName;
                
    }
    
    
    
    
}