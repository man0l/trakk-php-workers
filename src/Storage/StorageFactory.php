<?php
namespace Trakk\Storage;

use Trakk\Storage\LocalStorage;

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


class  StorageFactory
{
    
    static public function getStorage($type = 'local', $options = [])
    {
        switch($type)
        {
            default: 
                
                $storage = new LocalStorage();          
                
                if(isset($options['saveDir']))
                {
                    $storage->setSaveDir($options['saveDir']);
                }
        }
        
        
        if(!($storage instanceof AbstractStorage))
        {
            throw new Exception('The storage instance could not be created');
        }        
        
        if($options['content'])
        {
            $storage->setContent($options['content']);                    
        }
        
        
        return $storage;
    }
    
}