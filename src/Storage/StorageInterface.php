<?php

namespace Trakk\Storage;
/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

interface StorageInterface
{
    
    public function save();    
    
    /**
     * @desc Expects content to be an HTML string
     * @param string $content
     */
    public function setContent($content);
    
    /**
     * @desc getter for the content
     */
    public function getContent();
    
}