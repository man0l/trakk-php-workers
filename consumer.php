<?php

ini_set('allow_url_fopen', 1);

require_once __DIR__ . '/vendor/autoload.php';

use PhpAmqpLib\Connection\AMQPConnection;
 
$connection = new AMQPConnection('rabbit.trak.ink', 5672, 'trakink', 'trak12345%');
$channel = $connection->channel();

//$channel->queue_declare('myqueue', false, false, false, false);

echo ' [*] Waiting for messages. To exit press CTRL+C', "\n";

$callback = function($msg) {
  
  echo " [x] Received ", $msg->body, "\n";
  
  $jsonMsg = json_decode($msg->body);
  
  if(isset($jsonMsg) && is_object($jsonMsg) && isset($jsonMsg->url)) {  
 
        $unirest = new Unirest;
        
        try {
            $res = $unirest->get($jsonMsg->url);

            if(is_object($res) && $res instanceof \Unirest\HttpResponse) {

                $storage = StorageFactory::getStorage('local', [ 'content' => $res->raw_body ]);
                $msg->delivery_info['channel']->basic_ack($msg->delivery_info['delivery_tag']);

                print_r($storage);
            }
        
        } catch (Excepetion $e)
        {
            echo  $e->getMessage();
        }
    
    
  }
  
  
  
};

$channel->basic_consume('myqueue', '', false, false, false, false, $callback);

while(count($channel->callbacks)) {
    $channel->wait();
}


